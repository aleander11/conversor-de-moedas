package br.com.aleander.conversaodemoedas.Model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["id"], unique = true)])
class Calculo {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "moeda_desejada")
    var moeda_desejada: String? = null

    @ColumnInfo(name = "moeda_converter")
    var moeda_converter: String? = null

    @ColumnInfo(name = "quantidade")
    var quantidade = 0.0

    @ColumnInfo(name = "resultado")
    var resultado = 0.0
}