package br.com.azapfy.azapfyvolumetria.Configs.download

import android.app.Activity
import android.util.Log
import br.com.aleander.conversaodemoedas.DbController
import br.com.aleander.conversaodemoedas.Model.database.AppDatabase
import br.com.aleander.conversaodemoedas.Model.entity.Moedas
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


object Requisicoes {
    //Requisicao para buscar moedas
    fun request(url: String, context: Activity?) {
        if (context == null) {
            return
        }
        val novaMoeda = Moedas()
        val stringRequest = StringRequest(url, Response.Listener { response ->
            Thread {
                val moedas = JSONObject(response).getJSONObject("rates")
                for (i in moedas.keys()) {
                    novaMoeda.valor = moedas.get(i).toString().toDouble()
                    novaMoeda.moeda = i.toString()
                    val moeda = AppDatabase.use(context)!!.moedasDao()!!.buscarMoeda(i.toString())
                    Log.e("ss", moeda.moeda.toString())
                    if (moeda.moeda.toString() != i.toString() && moeda.valor.toString() != moedas.get(
                            i
                        ).toString()
                    ) {
                        AppDatabase.use(context)!!.moedasDao()!!.deletarMoeda(i.toString())
                        DbController.inserirMoeda(context, novaMoeda)
                    } else if (moeda.moeda.toString() == "null") {
                        DbController.inserirMoeda(context, novaMoeda)
                    }
                }
            }.start()
        }, Response.ErrorListener {
            Log.e("Erro requisicao inserir moeda", "Erro: $it")
        })
        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add(stringRequest)


    }
}

