package br.com.aleander.conversaodemoedas.Model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["id"], unique = true)])
class Moedas {
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "moeda")
    var moeda: String? = null

    @ColumnInfo(name = "valor")
    var valor = 0.0
}