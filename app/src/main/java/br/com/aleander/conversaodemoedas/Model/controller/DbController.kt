package br.com.aleander.conversaodemoedas

import android.content.Context
import android.util.Log
import br.com.aleander.conversaodemoedas.Model.database.AppDatabase
import br.com.aleander.conversaodemoedas.Model.entity.Moedas
import br.com.aleander.conversaodemoedas.Model.entity.Calculo


object DbController {

    //inseri moedas
    fun inserirMoeda(
        context: Context,
        novaMoeda: Moedas
    ): Boolean {
        try {
            AppDatabase.use(context)!!.moedasDao()!!.insert(novaMoeda)
            return true
        } catch (e: java.lang.Exception) {
            Log.e("Erro inseir moeda controller", e.toString())
            return false
        }
    }

    //inseri dados no historico
    fun inserirHistorico(
        context: Context,
        novoHistorico: Calculo
    ): Boolean {
        try {
            AppDatabase.use(context)!!.calculoDao()!!.insert(novoHistorico)
            return true
        } catch (e: java.lang.Exception) {
            Log.e("Erro inseir historico controller", e.toString())
            return false
        }
    }
}
