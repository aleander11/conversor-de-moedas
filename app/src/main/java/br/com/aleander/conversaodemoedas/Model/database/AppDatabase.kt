package br.com.aleander.conversaodemoedas.Model.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.aleander.conversaodemoedas.Model.dao.CalculoDao
import br.com.aleander.conversaodemoedas.Model.dao.MoedasDao
import br.com.aleander.conversaodemoedas.Model.entity.Calculo
import br.com.aleander.conversaodemoedas.Model.entity.Moedas

@Database(entities = [Moedas::class, Calculo::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun moedasDao(): MoedasDao?
    abstract fun calculoDao(): CalculoDao?

    //Cria o banco
    companion object {

        private const val DATABASE_NAME = "aplicativo-moedas"
        private var INSTANCE: AppDatabase? = null
        fun use(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DATABASE_NAME
                    )
                        .build()
            }
            return INSTANCE
        }
    }
}