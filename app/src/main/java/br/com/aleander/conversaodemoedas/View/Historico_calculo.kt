package br.com.aleander.conversaodemoedas.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.aleander.conversaodemoedas.Controller.Adapter.AdapterListHistorico
import br.com.aleander.conversaodemoedas.Model.database.AppDatabase
import br.com.aleander.conversaodemoedas.Model.entity.Calculo
import br.com.aleander.conversaodemoedas.R
import kotlinx.android.synthetic.main.activity_historico_calculo.*

class Historico_calculo : AppCompatActivity() {
    lateinit var mAdapter: AdapterListHistorico
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historico_calculo)
        view.layoutManager = LinearLayoutManager(applicationContext)
        Thread {
            var all = AppDatabase.use(applicationContext)!!.calculoDao()!!.buscarTodos()
            runOnUiThread {
                mAdapter =
                    AdapterListHistorico(
                        all!! as ArrayList<Calculo>,
                        applicationContext
                    )
                view.adapter = mAdapter
            }
        }.start()
        filto_lista.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                mAdapter.filter(s.toString())
            }
        })
        mAdapter =
            AdapterListHistorico(
                ArrayList(),
                applicationContext
            )

    }

}
