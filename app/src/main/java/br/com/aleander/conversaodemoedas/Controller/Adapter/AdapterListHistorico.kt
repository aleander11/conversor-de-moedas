package br.com.aleander.conversaodemoedas.Controller.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.aleander.conversaodemoedas.View.Historico_calculo
import br.com.aleander.conversaodemoedas.Model.entity.Calculo
import br.com.aleander.conversaodemoedas.Model.entity.Moedas
import br.com.aleander.conversaodemoedas.R
import kotlinx.android.synthetic.main.item_historico.view.*


class AdapterListHistorico(val lista: MutableList<Calculo>, val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {
    lateinit var mAdapter: Historico_calculo
    var items = ArrayList<Calculo>(lista)
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        p0.moeda_seleciona_value?.text = "Moeda a converter:" + items.get(p1).moeda_converter
        p0.moeda_seleciona_converter?.text = "Moeda desejada:" + items.get(p1).moeda_desejada
        p0.quantida_value?.text = "Valor:" + items.get(p1).quantidade.toString()
        p0.resultado_text?.text = "Resultado:" + items.get(p1).resultado.toString()
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_historico,
                p0,
                false
            )
        )
    }

    //filtrar lista
    fun filter(query: String) {
        items.clear()
        if (!query.isEmpty()) {
            for (calculosList in lista) {
                Log.e("11233", (calculosList).moeda_converter!!)
                if ((calculosList).moeda_converter!!.toUpperCase().contains(query.toUpperCase())
                    || (calculosList).moeda_desejada!!.toUpperCase().contains(query.toUpperCase())
                    || (calculosList).resultado.toString().toUpperCase()
                        .contains(query.toUpperCase())
                    || (calculosList).quantidade.toString().toUpperCase().contains(
                        query.toUpperCase()
                    )
                ) {
                    Log.e("3333", calculosList.quantidade.toString())
                    items.add(calculosList)
                }
            }
        } else {

            items.addAll(lista)
        }

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }
}


class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {

    override fun onLongClick(v: View?): Boolean {

        return false
    }

    val moeda_seleciona_value = itemView.moeda_seleciona_value
    val moeda_seleciona_converter = itemView.moeda_seleciona_converter
    val quantida_value = itemView.quantida_value
    val resultado_text = itemView.resultado_text


    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    override fun onClick(view: View) {

    }
}

