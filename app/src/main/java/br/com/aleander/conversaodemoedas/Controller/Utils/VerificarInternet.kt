package br.com.aleander.conversaodemoedas.Controller.Utils

import android.content.Context
import android.net.ConnectivityManager
object VerificarInternet {
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}
