package br.com.aleander.conversaodemoedas.View

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.aleander.conversaodemoedas.Controller.Utils.VerificarInternet
import br.com.aleander.conversaodemoedas.R
import br.com.azapfy.azapfyvolumetria.Configs.download.Requisicoes
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        Thread {
            //buscar moedas
            Requisicoes.request(
                "https://api.exchangeratesapi.io/latest", this
            )
        }.start()
        //botão de logar
        entrar_buttom.setOnClickListener {
            if (VerificarInternet.isOnline(this) == false) {
                Toast.makeText(
                    applicationContext, "Oops! Não há conexão com a internet. " +
                            "Verifique sua conexão e tente novamente.", Toast.LENGTH_SHORT
                ).show()
            } else if (email_fill.text.toString() == "") {
                Toast.makeText(
                    applicationContext, "Por favor, preencha o email", Toast.LENGTH_SHORT
                ).show()
            } else if (senha_fill.text.toString() == "") {
                Toast.makeText(
                    applicationContext, "Por favor, preencha a senha", Toast.LENGTH_SHORT
                ).show()
            } else {
                var i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
            }
        }
    }


}
