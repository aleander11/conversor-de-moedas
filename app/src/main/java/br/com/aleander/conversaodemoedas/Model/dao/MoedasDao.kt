package br.com.aleander.conversaodemoedas.Model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.aleander.conversaodemoedas.Model.entity.Moedas

@Dao
interface MoedasDao {
    @Insert
    fun insert(valores: Moedas?)

    @Query("SELECT moeda FROM Moedas ")
    fun buscarMoedas(): List<String?>?

    @Query("SELECT * FROM Moedas ")
    fun buscarTodos(): List<Moedas?>?

    @Query("SELECT * FROM Moedas WHERE moeda = :moeda1")
    fun buscarMoeda(
        moeda1: String
    ): Moedas

    @Query("DELETE  FROM  Moedas WHERE moeda = :moeda1")
    fun deletarMoeda(
        moeda1: String?
    )


    @Query("SELECT valor FROM Moedas WHERE moeda = :moeda1 OR moeda = :moeda2 ")
    fun buscarMoedasConta(
        moeda1: String,
        moeda2: String
    ): List<Double>


    @Query("DELETE FROM moedas")
    fun dropTabela()
}