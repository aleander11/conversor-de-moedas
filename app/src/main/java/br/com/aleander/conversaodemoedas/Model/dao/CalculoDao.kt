package br.com.aleander.conversaodemoedas.Model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.com.aleander.conversaodemoedas.Model.entity.Calculo

@Dao
interface CalculoDao {
    @Insert
    fun insert(valores: Calculo?)

    @Query("SELECT * FROM Calculo ")
    fun buscarTodos(): List<Calculo?>?
}