package br.com.aleander.conversaodemoedas.View

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import br.com.aleander.conversaodemoedas.DbController
import br.com.aleander.conversaodemoedas.Model.database.AppDatabase
import br.com.aleander.conversaodemoedas.Model.entity.Calculo
import br.com.aleander.conversaodemoedas.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prencherListas()
        //botao de finalizar calculo
        finaliza.setOnClickListener {
            if (quantidade_text.text.toString() == "") {

                Toast.makeText(
                    applicationContext, "Por favor, preencha o valor desejado", Toast.LENGTH_SHORT
                ).show()

            } else {

                var moeda_desejada = spinner_moeda_desejada.selectedItem.toString()
                var moeda_converter = spinner_tipo_moeda.selectedItem.toString()
                var quantidade = quantidade_text.text.toString().toDouble()
                Thread {
                    var converter_valor = AppDatabase.use(applicationContext)!!.moedasDao()!!
                        .buscarMoeda(moeda_converter)
                    var desejada_valor = AppDatabase.use(applicationContext)!!.moedasDao()!!
                        .buscarMoeda(moeda_desejada)

                    val resultado = converterValor(converter_valor.valor, desejada_valor.valor, quantidade)
                    val novoCalculo = Calculo()
                    novoCalculo.moeda_converter = moeda_converter
                    novoCalculo.moeda_desejada = moeda_desejada
                    novoCalculo.quantidade = quantidade
                    novoCalculo.resultado = resultado.toString().toDouble()
                    DbController.inserirHistorico(
                        applicationContext,
                        novoCalculo
                    )
                    runOnUiThread {
                        resultado_text.setText(resultado.toString())
                    }
                }.start()
            }
        }
        historico.setOnClickListener {
            var i = Intent(this, Historico_calculo::class.java)
            startActivity(i)
        }
    }

    //Prenche as listas da tela main
    private fun prencherListas() {
        Thread {
            var moedasList = AppDatabase.use(applicationContext)!!.moedasDao()!!.buscarMoedas()
            runOnUiThread {
                val moedas = ArrayAdapter(this, android.R.layout.simple_spinner_item, moedasList)
                moedas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner_moeda_desejada!!.adapter = moedas

                spinner_tipo_moeda!!.adapter = moedas
            }
        }.start()

    }

    //Faz a conta para conversão
    private fun converterValor(
        valor_converter: Double,
        taxa_cambio: Double,
        quantidade: Double
    ): Double {
        var resultado = (taxa_cambio * quantidade) / valor_converter
        resultado = resultado.toString().substring(0, 5).toDouble()
        return resultado
    }

}
